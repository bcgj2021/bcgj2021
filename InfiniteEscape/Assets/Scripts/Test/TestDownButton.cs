using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestDownButton : MonoBehaviour
{
    public BigNumber energyNumber;
    public Text energyText;
    private string[] unitAbbrevs = {"","K","M","B","T","aa","bb","cc","dd","ee","ff","gg","hh","ii","jj","kk","ll","mm","nn","oo","pp","qq","rr","ss","tt","uu","vv","ww","xx","yy","zz"};
    
    // Start is called before the first frame update
    void Start()
    {
        energyNumber = energyText.gameObject.GetComponent<TestEnergy>().energy;
        UpdateEnergyText(energyNumber.GetAbbreviated());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnButtonPress()
    {      
        energyNumber = energyNumber - 100000;
        //Debug.Log(energy.MainNumber);
        UpdateEnergyText(energyNumber.GetAbbreviated());
        Debug.Log("end of button press");
    }

    private void UpdateEnergyText(string newStringValue)
    {
        //format New Value string
        energyText.text = energyNumber.GetAbbreviated();
    }
}
