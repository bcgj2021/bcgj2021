public class Item
{
    public int id, level, baseEnergy;
    public string itemName;

    public Item() { }

    public Item(int id, string itemName, int baseEnergy, int level = 1)
    {
        this.id = id;
        this.itemName = itemName; 
        this.baseEnergy = baseEnergy;
        this.level = level;
    }
}