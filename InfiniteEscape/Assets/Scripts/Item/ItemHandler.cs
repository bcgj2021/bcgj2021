using System.Collections.Generic;
using UnityEngine;

public class ItemHandler : MonoBehaviour
{
    [SerializeField]
    private TextAsset itemDataAsset;

    private string[] itemNameDatas;
    private int[] baseEnergyDatas;

    [SerializeField]
    private GameObject itemSlotPrefab;

    [SerializeField]
    private Transform itemSlotParent;

    private ItemUI[] itemSlots; 
    private static List<int> inventory = new List<int>();

    void Start()
    {
        string[] itemDatas = itemDataAsset.ToString().Split(';');

        itemNameDatas = new string[itemDatas.Length];
        baseEnergyDatas = new int[itemDatas.Length];

        for (int i = 0; i < itemDatas.Length; i++)
        {
            itemNameDatas[i] = itemDatas[i].Split('-')[0].TrimStart();
            baseEnergyDatas[i] = int.Parse(itemDatas[i].Split('-')[1]);
        }

        itemSlots = new ItemUI[itemDatas.Length];
        for (int i = 0; i < itemDatas.Length; i++)
        {
            Item item = new Item(i, itemNameDatas[i], baseEnergyDatas[i]);

            GameObject itemSlot = Instantiate(itemSlotPrefab, itemSlotParent);
            
            itemSlots[i] = itemSlot.GetComponent<ItemUI>();
            itemSlots[i].SetInfo(item);
        }
    }

    public void ResetItems()
    {
        for (int i = 0; i < itemSlots.Length; i++)
        {
            if(inventory.Contains(itemSlots[i].item.id))
            {
                itemSlots[i].UpdateUI();
            }
        }
    }

    public static void AddItemToInventory(int id)
    {
        if (!inventory.Contains(id))
        {
            inventory.Add(id);
        }
    }
}