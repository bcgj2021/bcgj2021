using UnityEngine;
using UnityEngine.UI;

public class ItemUI : MonoBehaviour
{
    [SerializeField]
    private Text nameText, levelText, generatedEnergyText, levelUpText, unlockText;

    [SerializeField]
    private GameObject unlockObj;

    public Item item
    {
        get;
        private set;
    }

    private int generatedEnergy, levelUpCost, unlockCost;

    public void SetInfo(Item item, bool isUnclocked = false)
    {
        this.item = item;

        levelUpCost = (int)(item.baseEnergy * item.level * 1.2f);
        unlockCost = item.baseEnergy;
        generatedEnergy = item.baseEnergy * item.level;

        nameText.text = item.itemName;
        levelText.text = item.level.ToString();
        generatedEnergyText.text = (generatedEnergy < 1000 ? generatedEnergy.ToString() : new BigNumber(generatedEnergy, 0).GetAbbreviated()) + "/sec";
        levelUpText.text = (levelUpCost < 1000 ? levelUpCost.ToString() : new BigNumber(levelUpCost, 0).GetAbbreviated()) + " Energy";

        if (isUnclocked)
        {
            unlockText.text = "1 Energy";
        }
        else
        {
            unlockText.text = (unlockCost < 1000 ? unlockCost.ToString() : new BigNumber(unlockCost, 0).GetAbbreviated()) + " EXP";
        }
    }

    //Button functions
    public void OnLevelUpClick()
    {
        if (!unlockObj.activeSelf && Energy.instance.hasEnoughEnergy(levelUpCost))
        {
            Energy.instance.DecreaseEnergy(levelUpCost);

            item.level++;
            levelUpCost = (int)(item.baseEnergy * item.level * 1.2f);
            generatedEnergy = item.baseEnergy * item.level;

            Energy.instance.IncreaseEnergyProduceSpeed(generatedEnergy / 100);

            levelText.text = item.level.ToString();
            generatedEnergyText.text = (generatedEnergy < 1000 ? generatedEnergy.ToString() : new BigNumber(generatedEnergy, 0).GetAbbreviated()) + "/sec";
            levelUpText.text = (levelUpCost < 1000 ? levelUpCost.ToString() : new BigNumber(levelUpCost, 0).GetAbbreviated()) + " Energy";
        }
    }

    public void OnUnlockClick()
    {
        bool canUnlock = (unlockText.text == "1 Energy"? Energy.instance.hasEnoughEnergy(1) : Experience.instance.hasEnoughExp(unlockCost));
        if (canUnlock)
        {
            Experience.instance.DecreaseExp(unlockCost);
            Energy.instance.IncreaseEnergyProduceSpeed(generatedEnergy / 100);

            ItemHandler.AddItemToInventory(item.id);
            unlockObj.SetActive(false);
        }
    }

    public void UpdateUI()
    {
        unlockObj.SetActive(true);
        unlockText.text = "1 Energy";
    }
}