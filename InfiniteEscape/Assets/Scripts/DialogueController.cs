using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DialogueController : MonoBehaviour
{
    public static DialogueController instance;

    public Dictionary<string, Dialogue> mDialogues = new Dictionary<string, Dialogue>();

    // Start is called before the first frame update
    void Awake() //pseudo singleton
    {
        if (DialogueController.instance == null)
        {
            DialogueController.instance = this;
        }
        else if (DialogueController.instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        string fileData;
        string[] lines;
        string[] lineData;

        fileData = System.IO.File.ReadAllText((System.IO.Directory.GetCurrentDirectory() + "/Assets/Resources/Dialogue.csv"));
        lines = fileData.Split("\n"[0]);
        for (int i = 1; i < lines.Length; i++)// skip first line of keys
        {
            lineData = (lines[0].Trim()).Split(","[0]);
            Dialogue tempDialogue = new Dialogue(lineData[0], lineData[1], lineData[2].Replace('|', ','));
            mDialogues.Add(lineData[0],tempDialogue);
            
        }
        lineData = (lines[0].Trim()).Split(","[0]);
        


    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void PlayWithKey(string key)
    {
        //WIP
        mDialogues[key].Play();
    }
}

public class Dialogue
{
    public string Name;
    public string FileKey;
    public string Text;

    public Dialogue(string n, string fk, string t)
    {
        Name = n;
        FileKey = fk;
        Text = t;
    }

    public void Play()
    {
        //will do something eventually
    }
}
