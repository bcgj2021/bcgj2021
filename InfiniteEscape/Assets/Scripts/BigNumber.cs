using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigNumber
{
    private decimal mainNumber; //remains between -10 and 10
    private int extraDigits;
    private static decimal mainLimit = 10^12;
    private static string[] unitAbbrevs = { "", "K", "M", "B", "T", "aa", "bb", "cc", "dd", "ee", "ff", "gg", "hh", "ii", "jj", "kk", "ll", "mm", "nn", "oo", "pp", "qq", "rr", "ss", "tt", "uu", "vv", "ww", "xx", "yy", "zz" };

    public decimal MainNumber {
        get { return mainNumber; }
        set { mainNumber = value; }
    }

    public int ExtraDigits
    {
        get { return extraDigits; }
        set { extraDigits = value; }
    }

    public BigNumber(decimal mainNum, int extraDigs)
    {
        mainNumber = mainNum;
        mainNumber = decimal.Round(mainNumber, 13);
        extraDigits = extraDigs;
        this.Trim();
    }

    public BigNumber(float fNumber)
    {
        mainNumber = (decimal)fNumber;
        extraDigits = 0;
        this.Trim();

    }

    public BigNumber(decimal dNumber)
    {
        mainNumber = dNumber;
        extraDigits = 0;
        this.Trim();
    }
    public BigNumber(string stringNumber)
    {
        int extraLength = 0;
        string shortStringNumber = stringNumber;
        
        if (stringNumber.Length > 25) {
            shortStringNumber = stringNumber.Substring(0, 25);
            Debug.Log(shortStringNumber);
            extraLength = stringNumber.Length - 25;
        }

        mainNumber = System.Convert.ToDecimal(shortStringNumber);
        //Debug.Log(mainNumber+ ", " + extraDigits);
        extraDigits = extraLength;
        this.Trim();
    }

    public static BigNumber operator +(BigNumber a, BigNumber b)
    {
        Debug.Log("start to add");
        //if they're the same number of digits
        BigNumber newNumber;
        decimal newMain;
        int newExtraDigits;
        if (a.extraDigits == b.extraDigits)
        {
            newMain = a.mainNumber + b.mainNumber;
            newExtraDigits = a.extraDigits;
        }
        else if (Mathf.Abs(a.extraDigits-b.extraDigits)<25) // if they're close enough to be relevant
        {
            int difference = a.extraDigits - b.extraDigits;
            if (difference > 0) //|a|>|b|
            {
                decimal tempValue = b.mainNumber / (decimal)Mathf.Pow(10, difference);
                Debug.Log("difference: " + difference + " -> " + tempValue);
                newMain = a.mainNumber + tempValue;
                newExtraDigits = a.extraDigits;
            }
            else //|b|>|a|
            {
                decimal tempValue = a.mainNumber / (decimal)Mathf.Pow(10, difference);
                newMain = tempValue + b.mainNumber;
                newExtraDigits = b.extraDigits;
            }
        }
        else // if they're too far apart, just take the bigger one
        {
            newMain = (a.extraDigits > b.extraDigits ? a.extraDigits : b.extraDigits);
            newExtraDigits = Mathf.Max(a.extraDigits, b.extraDigits);
        }
        

        //newNumber.Trim();
        newNumber = new BigNumber(newMain, newExtraDigits);
        Debug.Log("end add function");
        return newNumber;
    }
    public static BigNumber operator +(BigNumber a, float b)
    {
        BigNumber newNumber = new BigNumber((decimal)b, 0);
        return (a + newNumber);
    }
    public static BigNumber operator +(BigNumber a, int b)
    {
        BigNumber newNumber = new BigNumber((decimal)b, 0);
        return (a + newNumber);
    }

    public static BigNumber operator -(BigNumber a, BigNumber b)
    {
        //if they're the same number of digits
        BigNumber newNumber;
        decimal newMain;
        int newExtraDigits;
        if (a.extraDigits == b.extraDigits)
        {
            newMain = a.mainNumber - b.mainNumber;
            newExtraDigits = a.extraDigits;
        }
        else if (Mathf.Abs(a.extraDigits - b.extraDigits) < 25) // if they're close enough to be relevant
        {
            int difference = a.extraDigits - b.extraDigits;
            if (difference > 0) //|a|>|b|
            {
                decimal tempValue = b.mainNumber / (decimal)Mathf.Pow(10, difference);
                newMain = a.mainNumber - tempValue;
                newExtraDigits = a.extraDigits;
            }
            else //|b|>|a|  - set to zero so it doesn't go <zero
            {
                Debug.Log("try to go negative - go to zero");
                newMain = 0;
                newExtraDigits = 0;
            }
        }
        else // if they're too far apart, just take the bigger one
        {
            newMain = (a.extraDigits > b.extraDigits ? a.extraDigits : b.extraDigits);
            newExtraDigits = Mathf.Max(a.extraDigits, b.extraDigits);
        }


        //newNumber.Trim();
        newNumber = new BigNumber(newMain, newExtraDigits);
        return newNumber;
    }
    public static BigNumber operator -(BigNumber a, float b)
    {
        BigNumber newNumber = new BigNumber((decimal)b, 0);
        return (a- newNumber);
    }

    public static BigNumber operator -(BigNumber a, int b)
    {
        BigNumber newNumber = new BigNumber((decimal)b, 0);
        return (a - newNumber);
    }

    public static BigNumber operator *(BigNumber a, BigNumber b)
    {
        decimal newMain = (a.MainNumber * b.mainNumber);
        int newExtraDigits = Mathf.Max(a.extraDigits, b.extraDigits);
        BigNumber newNumber = new BigNumber(newMain, newExtraDigits);
        //newNumber.Trim();

        return newNumber;
    }
    public static BigNumber operator *(BigNumber a, float b)
    {
        decimal newMain = a.MainNumber * ((decimal)b);
        int newExtraDigits = a.extraDigits;
        BigNumber newNumber = new BigNumber(newMain, newExtraDigits);
        //newNumber.Trim();

        return newNumber;
    }

    public static BigNumber operator *(BigNumber a, int b)
    {
        //Debug.Log("using int multiplier");
        decimal newMain = 0;
        checked
        {
            newMain = a.MainNumber * ((decimal)b);

        }
        int newExtraDigits = a.extraDigits;
        BigNumber newNumber = new BigNumber(newMain, newExtraDigits);
        //newNumber.Trim();

        return newNumber;

    }

    public static BigNumber operator /(BigNumber a, BigNumber b)
    {
        decimal newMain = a.MainNumber / b.mainNumber;
        int newExtraDigits = a.extraDigits - b.extraDigits;
        BigNumber newNumber = new BigNumber(newMain, newExtraDigits);
        //newNumber.Trim();

        return newNumber;
    }

    public static BigNumber operator /(BigNumber a, float b)
    {
        decimal newMain = a.MainNumber / (decimal)b;
        int newExtraDigits = a.extraDigits;
        BigNumber newNumber = new BigNumber(newMain, newExtraDigits);
       //newNumber.Trim();

        return newNumber;
    }

    public static BigNumber operator /(BigNumber a, int b)
    {
        decimal newMain = a.MainNumber / (decimal)b;
        int newExtraDigits = a.extraDigits;
        BigNumber newNumber = new BigNumber(newMain, newExtraDigits);
        //newNumber.Trim();

        return newNumber;
    }

    // > Is Bigger
    public static bool operator >(BigNumber a, BigNumber b)
    {
        bool isBigger = ((a.extraDigits > b.extraDigits)||(a.extraDigits==b.extraDigits && a.mainNumber > b.mainNumber));
        return isBigger;
    }
    public static bool operator >(BigNumber a, float b)
    {
        BigNumber tempNumber = new BigNumber(b);
        bool isBigger = ((a.extraDigits > (decimal)tempNumber.extraDigits) || (a.extraDigits == tempNumber.extraDigits && a.mainNumber > tempNumber.mainNumber));
        return isBigger;
    }
    public static bool operator >(BigNumber a, int b)
    {
        BigNumber tempNumber = new BigNumber((float)b);
        bool isBigger = ((a.extraDigits > (decimal)tempNumber.extraDigits) || (a.extraDigits == tempNumber.extraDigits && a.mainNumber > tempNumber.mainNumber));
        return isBigger;
    }
    public static bool operator >(BigNumber a, decimal b)
    {
        BigNumber tempNumber = new BigNumber(b);
        bool isBigger = ((a.extraDigits > (decimal)tempNumber.extraDigits) || (a.extraDigits == tempNumber.extraDigits && a.mainNumber > tempNumber.mainNumber));
        return isBigger;
    }
    // < Is Smaller
    public static bool operator <(BigNumber a, BigNumber b)
    {
        bool isSmaller = ((a.extraDigits < b.extraDigits) || (a.extraDigits == b.extraDigits && a.mainNumber < b.mainNumber));
        return isSmaller;
    }
    public static bool operator <(BigNumber a, float b)
    {
        BigNumber tempNumber = new BigNumber(b);
        bool isSmaller = ((a.extraDigits < (decimal)tempNumber.extraDigits) || (a.extraDigits == tempNumber.extraDigits && a.mainNumber < tempNumber.mainNumber));
        return isSmaller;
    }
    public static bool operator <(BigNumber a, int b)
    {
        BigNumber tempNumber = new BigNumber((float)b);
        bool isSmaller = ((a.extraDigits < (decimal)tempNumber.extraDigits) || (a.extraDigits == tempNumber.extraDigits && a.mainNumber < tempNumber.mainNumber));
        return isSmaller;
    }
    public static bool operator <(BigNumber a, decimal b)
    {
        BigNumber tempNumber = new BigNumber(b);
        bool isSmaller = ((a.extraDigits < (decimal)tempNumber.extraDigits) || (a.extraDigits == tempNumber.extraDigits && a.mainNumber < tempNumber.mainNumber));
        return isSmaller;
    }
    // == Is Equal
    public static bool operator ==(BigNumber a, BigNumber b)
    {
        bool isBigger = (a.extraDigits == b.extraDigits && a.mainNumber == b.mainNumber);
        return isBigger;
    }
    public static bool operator ==(BigNumber a, float b)
    {
        BigNumber tempNumber = new BigNumber(b);
        bool isEqual = (a.extraDigits == tempNumber.extraDigits && a.mainNumber == tempNumber.mainNumber);
        return isEqual;
    }
    public static bool operator ==(BigNumber a, int b)
    {
        BigNumber tempNumber = new BigNumber((float)b);
        bool isEqual = (a.extraDigits == tempNumber.extraDigits && a.mainNumber == tempNumber.mainNumber);
        return isEqual;
    }
    public static bool operator ==(BigNumber a, decimal b)
    {
        BigNumber tempNumber = new BigNumber(b);
        bool isEqual = (a.extraDigits == tempNumber.extraDigits && a.mainNumber == tempNumber.mainNumber);
        return isEqual;
    }
    // != Is Not Equal
    public static bool operator !=(BigNumber a, BigNumber b)
    {
        bool isNotEqual = (a.extraDigits != b.extraDigits || a.mainNumber != b.mainNumber);
        return isNotEqual;
    }
    public static bool operator !=(BigNumber a, float b)
    {
        BigNumber tempNumber = new BigNumber(b);
        bool isNotEqual = (a.extraDigits != tempNumber.extraDigits || a.mainNumber != tempNumber.mainNumber);
        return isNotEqual;
    }
    public static bool operator !=(BigNumber a, int b)
    {
        BigNumber tempNumber = new BigNumber((float)b);
        bool isNotEqual = (a.extraDigits != tempNumber.extraDigits || a.mainNumber != tempNumber.mainNumber);
        return isNotEqual;
    }
    public static bool operator !=(BigNumber a, decimal b)
    {
        BigNumber tempNumber = new BigNumber(b);
        bool isNotEqual = (a.extraDigits != tempNumber.extraDigits || a.mainNumber != tempNumber.mainNumber);
        return isNotEqual;
    }


    public string GetAbbreviated()
    {
        //Debug.Log("start of abbrev function");
        
        int numLength = extraDigits;//(int)Mathf.Log10((float)mainNumber)+extraDigits;
        int offsetUnits = (numLength % 3);
        
        float tempValue = (float)mainNumber; //((float)mainNumber) / (Mathf.Pow(10, numLength));
        string abbreviatedValue;
        //Debug.Log(numLength

        string unitString = unitAbbrevs[Mathf.FloorToInt(Mathf.Clamp(numLength / 3, 0, unitAbbrevs.Length - 1))];
        
        if (extraDigits >= 3)
        {
            tempValue *= Mathf.Pow(10, offsetUnits);
            abbreviatedValue = tempValue + "";
            //if (abbreviatedValue.Length >= 3)
            //{
            //    abbreviatedValue = (tempValue + "").Substring(0, 4);

            //}
        }
        else
        {
            abbreviatedValue = mainNumber * (decimal)Mathf.Pow(10, extraDigits)+"";
        }
        abbreviatedValue += unitString;
        //Debug.Log("end of abbrev function");
        //Do not uncomment the below comment for big equations - useful for debug but breaks larger numbers
        //Debug.Log(MainNumber *(decimal)Mathf.Pow(10,extraDigits) + " -> " + " Number: " + mainNumber + " * 10^" + extraDigits + " (" + tempValue + ", " + offsetUnits + ")");// (float)(mainNumber*(decimal)Mathf.Pow(10,extraDigits)));
        return abbreviatedValue;
    }

    private void Trim ()
    {
        //Debug.Log("start of trim function");
        bool isPositive = (mainNumber >= 0);
        decimal tempValue = isPositive? mainNumber : -mainNumber; // abs value
        if (tempValue >= 10)
        {
            //divide by 10 correct number of times and increment extraDigits accordingly
            int digitsAboveLimit = Mathf.FloorToInt(Mathf.Log10((float)(tempValue)));
            tempValue = tempValue / (decimal)(Mathf.Pow(10, digitsAboveLimit));
            //Debug.Log(tempValue);
            //Debug.Log("Digits above limit: " + digitsAboveLimit);
            extraDigits += digitsAboveLimit;
        }
        else if (tempValue < 1 && tempValue > 0)
        {
            //divide by 10 correct number of times and increment extraDigits accordingly
            int digitsAboveLimit = Mathf.FloorToInt(Mathf.Log10((float)(tempValue)));
            tempValue = tempValue / (decimal)(Mathf.Pow(10, digitsAboveLimit));
            Debug.Log(tempValue);
            Debug.Log("Digits above limit: " + digitsAboveLimit);
            extraDigits += digitsAboveLimit;
        }
        mainNumber = tempValue;//*(isPositive ? 1 : -1);
        Debug.Log("Number: " + mainNumber + " * 10^" + extraDigits);// (float)(mainNumber*(decimal)Mathf.Pow(10,extraDigits)));
        //Debug.Log("end of the trim function");
    }

}
