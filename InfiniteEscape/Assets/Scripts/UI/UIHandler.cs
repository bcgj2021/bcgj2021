using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{
    private GameObject activeUIObj;

    //To control color of texts
    [SerializeField]
    private Text[] buttonTexts;

    [SerializeField]
    private Text[] pageTexts;

    // Button Functions
    public void OpenUI(GameObject uiObj)
    {
        if (activeUIObj)
        {
            activeUIObj.SetActive(false);
        }

        uiObj.SetActive(true);
        activeUIObj = uiObj;
    }

    public void CloseUI(GameObject uiObj)
    {
        uiObj.SetActive(false);
    }

    public void ChangeButtonTextColor(Text text)
    {
        ChangeTextArrayColor(buttonTexts, text);
    }

    public void ChangePageTextColor(Text text)
    {
        ChangeTextArrayColor(pageTexts, text);
    }

    private void ChangeTextArrayColor(Text[] texts, Text clickedText)
    {
        for (int i = 0; i < texts.Length; i++)
        {
            if (texts[i] == clickedText)
            {
                clickedText.color = new Color(.6f, .4f, .15f);
            }
            else
            {
                texts[i].color = Color.white;
            }
        }
    }
}