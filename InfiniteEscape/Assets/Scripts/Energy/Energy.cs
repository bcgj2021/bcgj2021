using UnityEngine;
using UnityEngine.UI;

public class Energy : MonoBehaviour
{
    public static Energy instance;

    private decimal energy;
    private BigNumber energyBN;

    private float energyProduceSpeed = 1;

    private const float ENERGY_PRODUCE_REQUIRED_TIME = 1;
    private float currentTime = ENERGY_PRODUCE_REQUIRED_TIME;

    [SerializeField]
    private Text energyText, energyProduceSpeedText;

    void Start()
    {
        instance = this;
        
        SetEnergyText();
        energyProduceSpeedText.text = "x" + energyProduceSpeed.ToString();
    }

    void Update()
    {
        if(currentTime <= 0)
        {
            energy++;

            currentTime = ENERGY_PRODUCE_REQUIRED_TIME;
            SetEnergyText();
        }
        else
        {
            currentTime -= Time.deltaTime * energyProduceSpeed;
        }
    }

    public decimal GetEnergy()
    {
        return energy;
    }

    public bool hasEnoughEnergy(int amount)
    {
        return energy > amount;
    }

    public void DecreaseEnergy(int amount)
    {
        if(energy < amount)
        {
            energy = 0;
        }
        else
        {
            energy -= amount;
        }

        SetEnergyText();
    }

    public void IncreaseEnergyProduceSpeed(float increasement)
    {
        energyProduceSpeed += increasement;
        energyProduceSpeedText.text = energyProduceSpeed.ToString();
    }

    private void SetEnergyText()
    {
        if(energy < 1000)
        {
            energyText.text = energy.ToString();
        }
        else
        {
            energyBN = new BigNumber(energy, 0);

            if(energyBN.ExtraDigits < 10)
            {
                energyText.text = energyBN.GetAbbreviated();
            }
        }
    }

    public void Reset()
    {
        energy = 0;
        energyProduceSpeed = 1;

        SetEnergyText();
        energyProduceSpeedText.text = "x" + energyProduceSpeed.ToString();
    }
}