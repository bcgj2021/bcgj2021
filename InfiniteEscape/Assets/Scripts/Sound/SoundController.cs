using UnityEngine;
using UnityEngine.UI;

public class SoundController : MonoBehaviour
{
    private AudioSource[] audioSources;

    [SerializeField]
    private Button onButton, offButton;

    void Start()
    {
        audioSources = FindObjectsOfType<AudioSource>(true);
    }

    public void Unmute()
    {
        for (int i = 0; i < audioSources.Length; i++)
        {
            if (audioSources[i] != MusicController.musicAud)
            {
                audioSources[i].mute = false;
            }
        }

        onButton.interactable = false;
        offButton.interactable = true;
    }

    public void Mute()
    {
        for (int i = 0; i < audioSources.Length; i++)
        {
            if (audioSources[i] != MusicController.musicAud)
            {
                audioSources[i].mute = true;
            }
        }

        onButton.interactable = true;
        offButton.interactable = false;
    }
}