using UnityEngine;

public class MusicController : MonoBehaviour
{
    public static AudioSource musicAud
    {
        get;
        private set;
    } 
    
    void Start()
    {
        musicAud = GetComponent<AudioSource>();    
    }

    public void OnValueChanged(float value)
    {
        musicAud.volume = value;
    }
}
