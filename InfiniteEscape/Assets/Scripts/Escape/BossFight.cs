using UnityEngine;
using UnityEngine.UI;

public class BossFight : MonoBehaviour
{
    [SerializeField]
    private TextAsset bossDataAsset;

    [SerializeField]
    private Text bossHPText, expGainText, dpsText, bossNameText;

    [SerializeField]
    private Sprite[] stageBackgrounds, bossPhotos;

    [SerializeField]
    private Image background, bossImg;

    private string[] bossNames;
    private decimal[] bossHealths;

    private int bossRound;

    public static BossFight instance;

    private Countdown countdown;

    private decimal bossInitialHealth, bossHealth;
    private int totalExpGain, addedExpGain;

    private bool isFightEnded;

    void Start()
    {
        instance = this;
        countdown = GetComponent<Countdown>();

        string[] bossDatas = bossDataAsset.ToString().Split(';');

        bossNames = new string[bossDatas.Length];
        bossHealths = new decimal[bossDatas.Length];

        for (int i = 0; i < bossDatas.Length; i++)
        {
            bossNames[i] = bossDatas[i].Split('-')[0].TrimStart();
            bossHealths[i] = decimal.Parse(bossDatas[i].Split('-')[1]);
        }

        UpdateBossInfo();
        Reset();
    }

    public void UpdateBossInfo()
    {
        bossInitialHealth = bossHealths[bossRound];
        bossHealth = bossInitialHealth;
    }

    public bool Hit()
    {
        dpsText.text = "damage per second: "
                        + (Energy.instance.GetEnergy() < 1000 ? Energy.instance.GetEnergy().ToString() : new BigNumber(Energy.instance.GetEnergy(), 0).GetAbbreviated());

        //Health check
        if (bossHealth - Energy.instance.GetEnergy() <= 0)
        {
            isFightEnded = true;
            totalExpGain = (int)(bossInitialHealth / 100);
            Awaken.instance.AddAwakenExp(totalExpGain);

            bossHPText.text = "0/" + new BigNumber(bossInitialHealth, 0).GetAbbreviated();

            if(bossRound + 1 >= stageBackgrounds.Length)
            {
                bossRound = 0;
            }
            else
            {
                bossRound++;    
            }

            StopFight();
        }
        else
        {
            bossHealth -= Energy.instance.GetEnergy();
            totalExpGain = (int)((bossInitialHealth - bossHealth) / 100);
            bossHPText.text = (bossHealth < 1000 ? bossHealth.ToString() : new BigNumber(bossHealth, 0).GetAbbreviated()) + "/" + new BigNumber(bossInitialHealth, 0).GetAbbreviated();
        }

        Experience.instance.IncreaseExp(totalExpGain - addedExpGain);
        addedExpGain = totalExpGain;

        expGainText.text = "EXP earned: " + (totalExpGain < 1000 ? totalExpGain.ToString() : new BigNumber(totalExpGain, 0).GetAbbreviated());

        return isFightEnded;
    }

    public void StopFight()
    {
        UpdateBossInfo();
    }

    public void Reset()
    {
        bossNameText.text = bossNames[bossRound];
        bossHPText.text = new BigNumber(bossHealth, 0).GetAbbreviated() + "/" + new BigNumber(bossInitialHealth, 0).GetAbbreviated();
        dpsText.text = "damage per second: ";
        expGainText.text = "EXP earned: ";

        isFightEnded = false;
        totalExpGain = addedExpGain = 0;
        
        if (bossRound < stageBackgrounds.Length)
        {
            background.sprite = stageBackgrounds[bossRound];
            bossImg.sprite = bossPhotos[bossRound];
        }
    }
}
