using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    [SerializeField]
    private Button button;

    [SerializeField]
    private Text text;

    private const int COUNTDOWN_DEFAULT = 20;
    private int countDown = COUNTDOWN_DEFAULT;
    private float currentTime = 1;

    void Start()
    {
        button.onClick.AddListener(OnClick);
    }

    void Update()
    {
        if (!button.interactable)
        {
            if (currentTime <= 0)
            {
                bool isFightEnded = BossFight.instance.Hit();

                if (isFightEnded)
                {
                    Stop();
                }
                else
                {
                    countDown--;

                    if (countDown <= 0)
                    {
                        text.text = "DEFEAT";
                        button.interactable = true;

                        BossFight.instance.StopFight();
                    }
                    else
                    {
                        text.text = "0:" + (countDown < 10 ? "0" : "") + countDown; // when @countdown is one digit number, add "0" to make "0:0(@countdown)"
                    }

                    currentTime = 1;
                }
            }
            else
            {
                currentTime -= Time.deltaTime;
            }
        }
    }

    private void OnClick()
    {
        if (text.text == "VICTORY" || text.text == "DEFEAT")
        {
            countDown = COUNTDOWN_DEFAULT;
            currentTime = 1;

            BossFight.instance.Reset();
            
            text.text = "ESCAPE";
        }
        else
        {
            // Fight starts
            text.text = "0:" + (countDown < 10 ? "0" : "") + countDown; // when @countdown is one digit number, add "0" to make "0:0(@countdown)"
            button.interactable = false;
        }
    }

    public void Stop()
    {
        text.text = "VICTORY";
        button.interactable = true;
    }
}
