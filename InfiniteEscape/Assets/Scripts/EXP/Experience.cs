using UnityEngine;
using UnityEngine.UI;

public class Experience : MonoBehaviour
{
    public static Experience instance;

    public decimal exp
    {
        get;
        private set;
    }

    [SerializeField]
    private Text expText;

    void Start()
    {
        instance = this;

        expText.text = (exp < 1000? exp.ToString() : new BigNumber(exp, 0).GetAbbreviated());
    }

    public void IncreaseExp(decimal amount)
    {
        exp += amount;
        expText.text = (exp < 1000? exp.ToString() : new BigNumber(exp, 0).GetAbbreviated());
    }
    
    public void DecreaseExp(decimal amount)
    {
        if(exp - amount < 0)
        {
            exp = 0;
        }
        else
        {
            exp -= amount;
        }

        expText.text = (exp < 1000? exp.ToString() : new BigNumber(exp, 0).GetAbbreviated());
    }

    public bool hasEnoughExp(decimal amount)
    {
        return exp >= amount;
    }
}