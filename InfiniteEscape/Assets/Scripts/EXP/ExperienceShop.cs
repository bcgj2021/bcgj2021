using UnityEngine;
using UnityEngine.UI;

public class ExperienceShop : MonoBehaviour
{
    private const decimal UPGRADE_AMOUNT = .01m;
    private const int UPGRADE_COST = 10;

    //CP means CustomPurchase
    private decimal energyCPAmount, awakenCPAmount;
    private int energyCPCost, awakenCPCost;

    public Text energyCPText, awakenCPText;

    void Start()
    {
        energyCPAmount = awakenCPAmount = UPGRADE_AMOUNT;
        energyCPCost = awakenCPCost = UPGRADE_COST;
    }

    // CustomPurchase Inputfield Functions
    public void OnEnergyCPChange(string input)
    {
        if (input != "")
        {
            int upgradeCount = int.Parse(input);

            if (upgradeCount != 0)
            {
                energyCPAmount = UPGRADE_AMOUNT * upgradeCount;
                energyCPCost = UPGRADE_COST * upgradeCount;

                energyCPText.text = "+" + (energyCPAmount < 1000? energyCPAmount.ToString() : new BigNumber(energyCPAmount, 0).GetAbbreviated()) 
                                        + " for " + (energyCPCost < 1000? energyCPCost.ToString() : new BigNumber(energyCPCost, 0).GetAbbreviated());
            }
        }
    }

    public void OnAwakenCPChange(string input)
    {
        if (input != "")
        {
            int upgradeCount = int.Parse(input);

            if (upgradeCount != 0)
            {
                awakenCPAmount = UPGRADE_AMOUNT * upgradeCount;
                awakenCPCost = UPGRADE_COST * upgradeCount;

                awakenCPText.text = "+" + (awakenCPAmount < 1000? awakenCPAmount.ToString() : new BigNumber(awakenCPAmount, 0).GetAbbreviated()) 
                                        + " for " + (awakenCPCost < 1000? awakenCPCost.ToString() : new BigNumber(awakenCPCost, 0).GetAbbreviated());
            }
        }
    }

    // Button Functions
    public void OnEnergyClick(bool customBuy)
    {
        if (customBuy)
        {
            if (Experience.instance.hasEnoughExp(energyCPCost))
            {
                Experience.instance.DecreaseExp(energyCPCost);
                Energy.instance.IncreaseEnergyProduceSpeed((float)energyCPAmount);
            }
        }
        else
        {
            if (Experience.instance.hasEnoughExp(UPGRADE_COST))
            {
                Experience.instance.DecreaseExp(UPGRADE_COST);
                Energy.instance.IncreaseEnergyProduceSpeed((float)UPGRADE_AMOUNT);
            }
        }
    }

    public void OnAwakenClick(bool customBuy)
    {
        if (customBuy)
        {
            if (Experience.instance.hasEnoughExp(awakenCPCost))
            {
                Experience.instance.DecreaseExp(awakenCPCost);
                Awaken.instance.IncreaseAwakenExpMultiplier((float)awakenCPAmount);
            }
        }
        else
        {
            if (Experience.instance.hasEnoughExp(UPGRADE_COST))
            {
                Experience.instance.DecreaseExp(UPGRADE_COST);
                Awaken.instance.IncreaseAwakenExpMultiplier((float)UPGRADE_AMOUNT);
            }
        }
    }
}