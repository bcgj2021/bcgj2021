using UnityEngine;
using UnityEngine.UI;

public class Awaken : MonoBehaviour
{
    public static Awaken instance;

    private decimal awakenExp;
    public float awakenExpMultiplier
    {
        get;
        private set;
    }

    [SerializeField]
    private Text awakenExpMultiplierText;
    
    [SerializeField]
    private ItemHandler itemHandler;

    void Start()
    {
        instance = this;

        awakenExpMultiplier = 1;
        awakenExpMultiplierText.text = "x" + (awakenExpMultiplier < 1000? awakenExpMultiplier.ToString() : new BigNumber((decimal)awakenExpMultiplier, 0).GetAbbreviated());
    }
    
    public void IncreaseAwakenExpMultiplier(float increasemet)
    {
        awakenExpMultiplier += increasemet;
        awakenExpMultiplierText.text = "x" + awakenExpMultiplier;
    }

    public void AddAwakenExp(decimal exp)
    {
        awakenExp += exp * (decimal)awakenExpMultiplier;
    }

    // Button functions
    public void WriteAwakenExp(Text text)
    {
        text.text = (awakenExp < 1000? awakenExp.ToString() : new BigNumber(awakenExp, 0).GetAbbreviated());
    }
    
    public void OnWakeMeUpClick()
    {
        Experience.instance.IncreaseExp(awakenExp);
        awakenExp = 0;

        itemHandler.ResetItems();
        Energy.instance.Reset();
    }
}