using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class BossManager: MonoBehaviour
{
    public List<Boss> mBossList = new List<Boss>();
    private float roundMultiplier = 1.1f;
    void Awake()
    {
        //load all the boss types
        string fileData = System.IO.File.ReadAllText((System.IO.Directory.GetCurrentDirectory() + "/Assets/Resources/Bosses.csv"));
        string[] lines = fileData.Split("\n"[0]);
        

        for (int i = 1; i < lines.Length; i++) //start on 1 to avoid headers
        {
            //Debug.Log(lines[i]);
            string[] lineData = (lines[i].Trim()).Split(","[0]);

            //for (int j = 0; j < lineData.Length; j++)
            //{
            //    Debug.Log(lineData[j]);
            //}
            Boss tempBoss = new Boss(int.Parse(lineData[0]), lineData[1], lineData[2], new BigNumber(lineData[3]));
            //Debug.Log(tempBoss);
            mBossList.Add(tempBoss);
            //Debug.Log(tempBoss.BaseHealth.GetAbbreviated());
        }
        
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public class Boss
{
    private int mId;
    private string mName;
    private string mRoomName;
    private BigNumber mBaseHealth;


    public int ID {  get {  return mId; } }
    public string Name { get { return mName; } }
    public string RoomName { get { return mRoomName; } }
    public BigNumber BaseHealth { get { return mBaseHealth; } }


    public Boss()
    {
        mId = 0;

    }
    public Boss(int id, string n, string rn, BigNumber health)
    {
        mId = id;
        mName = n;
        mRoomName = rn;
        mBaseHealth = health;
    }


}
